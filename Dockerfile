# Cette commande cree un calque a partir de l'image Docker pour le JDK ou la JRE. Pour Java 8, il faut, si JDK (FROM openjdk:8-jdk-alpine ), sinon si JRE (FROM openjdk:8-jre-alpine)
# Pour Java 11  (JDK : openjdk:11-jdk-slim)
FROM adoptopenjdk/openjdk11:alpine-jre

# Auteur de l'image 
MAINTAINER oviok

ARG APP_VERSION=0.0.1-SNAPSHOT

# indique à Docker de créer un nouveau répertoire de travail dans l'image appelée "poo-process-sandbox" sous "opt". Toutes les autres commandes seront exécutées à partir de ce répertoire.
WORKDIR /opt/poo-process-sandbox/lib

# Copier l'archive executable
COPY ./target/poo-process-sandbox-${APP_VERSION}.jar /opt/poo-process-sandbox/lib/poo-process-sandbox.jar

EXPOSE 8888
ENTRYPOINT ["java", "-jar", "/opt/poo-process-sandbox/lib/poo-process-sandbox.jar"]


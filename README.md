# poo-process-sandbox

![](https://img.shields.io/badge/build-success-brightgreen.svg)

Dans ce projet plusieurs sujets sont traités/abordés tels que :

## 1°) Construire un projet Java avec Spring IO Platform : 

la plate-forme utilise la prise en charge de Maven pour la gestion des dépendances pour fournir des versions de dépendances au build de l'application
Deux opptions possibles :
```xml
	<!-- BOM dependencies management si on utilise pas BOM Parent -->
	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>io.spring.platform</groupId>
				<artifactId>platform-bom</artifactId>
				<version>${spring.platform.bom.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>
```
avec par exmple :  <spring.platform.bom.version>Cairo-SR4</spring.platform.bom.version> ou bien
```xml
	<!-- ============ BOM Parent : si on utilise pas BOM dependencies management ============ -->
	<parent>
		<groupId>io.spring.platform</groupId>
		<artifactId>platform-bom</artifactId>
		<version>Cairo-SR8</version>
		<relativePath />
	</parent>
```
## 2°) Construire un projet Java avec Spring Boot : tout comme Spring IO Platform deux options 
```xml
	<!-- BOOT dependencies management si on utilise pas BOOT Parent -->
	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-dependencies</artifactId>
				<version>${spring.boot.release.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>
```
avec par exemple :  <spring.boot.release.version>2.1.9.RELEASE</spring.boot.release.version> ou bien 
```xml
	<!-- ============ BOM Parent : si on utilise pas BOM dependencies management ============ -->
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.2.6.RELEASE</version>
		<relativePath /> <!-- lookup parent from repository -->
	</parent>
```
## 3°) Gestion des propriétés externalisées
	
- Customisation : utilisation de @ConfigurationProperties (contrairement à @Value, les expressions SpEL ne sont pas évaluées car les valeurs de propriété sont externalisées)
- Chargement et exploitation et tests : ConfigurableEnvironment uniquement dans le cadre de ce projet (permet de récupérer la propriété voulue au moent voulu, pas de connaissance à la base de l'ensemble des propriétés à utiliser dans le cadre du projet)

## 4°) La Gestion des exceptions

Les exceptions sont des événements qui se produisent pendant l'exécution de programmes qui perturbent le flux normal d'instructions (par exemple, division par zéro, accès au tableau hors des limites, etc.).
En Java, une exception est un objet qui encapsule un événement d'erreur survenu dans une méthode et contient :

- Informations sur l'erreur, y compris son type
- L'état du programme lorsque l'erreur s'est produite
-  En option, d'autres informations personnalisées
-  Les objets d'exception peuvent être lancés et capturés
Les exceptions sont utilisées pour indiquer de nombreux types différents de conditions d'erreur.

### 4-1°) Erreurs JVM : 
généralement des situations anormales dans la JVM, ce sont des objets de la classe java.lang.Error. 
	Exemples : OutOfMemoryError, StackOverflowError, LinkageError, Java.lang.NoClassDefFoundError, ava.lang.UnSupportedClassVersionError

### 4-2°) Erreurs système : 
généralement des situations anormales de type système (technique). Ce sont des objets de la classe java.lang.Exception. Checked ou compile-time exceptions (sont vérifiées à la compilation --> donc explicites), elles doivent être déclarées dans la signature de la méthode après le mot clé throws.	
Exemples : FileNotFoundException, IOException, SocketTimeoutException, SQLException, InvocationTargetException, ClassNotFoundException

### 4-3°) Erreurs de programmation : 
généralement des situations anormales de type applicatif/métier/fonctionnel (Business). Ce sont des objets de la classe java.lang.RuntimeException. Unchecked (ne sont pas vérifiées à la compilation --> implicites). Ce type d’exceptions peut être levé même sans déclaration préalable dans le throws.
Exemples : NullPointerException, IndexOutOfBoundsException, ArrayIndexOutOfBoundsException, ArithmeticException, ClassCastException, IllegalArgumentException,UnsupportedOperationException, IllegalStateException, NumberFormatException

## 5°) Accès statique aux beans Spring : Obtenir l'accès statique aux beans Spring à partir du contexte d'application (ApplicationContext)

Le contexte d'application (ApplicationContext) est au cœur du Framework Spring . Il offre quelques fonctionnalités différentes:
- Charger les ressources de fichiers de manière générique;			
- Publier des événements aux auditeurs enregistrés;			
- Résoudre les messages, soutenir l'internationalisation;			
- Il a des méthodes de fabrique de bean (Bean Factory) pour accéder aux composants d'application

Deux solutions sont traitées dans le cadre de ce projet :
- Solution à base de proxy : 

pour gérer le problème de timing et de NPE. Car il est poosible d'utiliser la méthode statique avant que ApplicationContext ne soit câblé automatiquement. Cela ferait planter le système car on ne 
peut pas appeler une méthode sur une référence pointant vers null. ApplicationContext sera éventuellement câblé automatiquement, généralement en quelques secondes, voire en millisecondes, après le démarrage de l'application. 
Mais nous ne pouvons pas non plus vraiment empêcher quiconque d'invoquer la méthode statique pour obtenir le bean avant que cela ne se produise.
Pour éviter le problème de synchronisation, nous devons combler le temps entre ces deux événements. Ce que nous pourrions faire est de fournir un objet 
temporaire, donc notre getBean() statique renvoie au moins quelque chose. Cela peut être réalisé en utilisant un objet Proxy, 
qui fait partie de la spécification Java
- Solution à base de Singleton

## 6°) Containerisation : Docker
Docker est une technologie de conteneurisation qui vous permet de créer une image contenant votre application et toutes les dépendances nécessaires à son exécution. 
L'image est un artefact déployable et peut être utilisée pour exécuter des conteneurs sur n'importe quelle machine virtuelle ou physique sur laquelle Docker est installé. 

### 6-1°) Le fichier Dockerfile : 
c'est un document texte qui contient toutes les instructions pour créer une image Docker. L'utilisation du jeu d'instruction du fichier Dockerfile permet entre autres de :
- Construire l'archive exécutable de l'application
- créer des répertoires, 
- copier des fichiers,
- exécuter des commandes, 
- faire l'installation de l'application,
- etc ...

#### 6-1-1°) Créer l'image avec espace de travail : le mot clé WORKDIR
Le mot clé indique à Docker de créer un nouveau répertoire de travail dans l'image. Les commandes de création et d'exécution de l'image sont les suivantes:
- Création : exécuter l'une des commandes
```sh
$ docker image build -f Dockerfile -t poo-process-sandbox:latest --rm=true .
$ docker image build --file=Dockerfile --tag=poo-process-sandbox:latest --rm=true .
```
- Exécution : exécuter l'une des commandes
```sh
$ docker container run -p 8888:8888 poo-process-sandbox
$ docker run -p 8888:8888 poo-process-sandbox
```
#### 6-1-2°) Créer l'image sans espace de travail : 
- Contenu du Dockerfile :
```dockerfile
# Cette commande cree un calque a partir de l'image Docker pour le JDK ou la JRE. Pour Java 8, il faut:
# Si JDK (FROM openjdk:8-jdk-alpine )
# Sinon si JRE (FROM openjdk:8-jre-alpine)
# Pour Java 11  (JDK : openjdk:11-jdk-slim)
FROM adoptopenjdk/openjdk11:alpine-jre
# Auteur de l'image 
MAINTAINER oviok
ARG APP_VERSION=0.0.1-SNAPSHOT
# Copier l'archive executable
ADD ./target/poo-process-sandbox-${APP_VERSION}.jar  poo-process-sandbox.jar
EXPOSE 8888
ENTRYPOINT ["sh", "-c", "java -jar poo-process-sandbox.jar"]
```
- Création : exécuter l'une des commandes
```sh
$ docker image build -f Dockerfile-workdir -t poo-process-sandbox:latest --rm=true .
$ docker image build --file=Dockerfile-workdir --tag=poo-process-sandbox:latest --rm=true .
```
- Exécution : exécuter l'une des commandes
```sh
$ docker container run -p 8888:8888 poo-process-sandbox
$ docker run -p 8888:8888 poo-process-sandbox
```
## 7°) Containerisation : Docker Compose

### 7-1°) Le fichier docker-compose.yml : 
C'est un fichier de composotion qui est utilisé pour agréger tous les "Dockerfile" afin de générer les nevironnement requis pour l'exécution de chaque application ou service.
LEs entrées importantes du fichier de composition sont les suivantes :
- version : champ obligatoire permettant de maintenir maintenir la version du format Docker Compose.
- services: chaque entrée définit le conteneur à générer.
- build: si mentionné, Docker Compose doit créer une image à partir du Dockerfile donné.
- image: le nom de l'image qui sera créée.
- networks : le nom du réseau à utiliser. Ce nom doit être présent dans la section réseaux.
- links : cela créera un lien interne entre le service et le service mentionné.
- depends : cela est nécessaire pour maintenir l'ordre.
 
### 7-2°) Démarrage: 
```sh
$ docker-compose up
# Construire tous les services mais ne pas les exécuter
$ docker-compose build 
# Construire tous les services et les exécuter
$ docker-compose up --build 
```
### 7-3°) Arrêt: 
```sh
$ docker-compose down
```
## 8°) Créer et transférer l'image Docker dans le registre GitLab
- Se connecter au régistre GitLab avec difénition variables pour les ifentifiants :
```sh
# Définir et renseigner les paramètres ou dientifiants de connexion à GitLab, il demande le mot de passe
$ CI_REGISTRY_USER=<username_ou_email_connxaion_gitlab>
$ CI_REGISTRY=<hostname_ou_url_regisyte_gitlab>
$ docker login -u ${CI_REGISTRY_USER} ${CI_REGISTRY}
```
- Se connecter au régistre GitLab sans défintion de variables :
```sh
# Dans cette procédure, les login et mot de passe sont à renseigner
$ docker login registry.gitlab.com
```
- Création de l'image et pull dans le registre
```sh
# Se connecter et exécuter les commandes ci-dessous : 
$ docker image build -f Dockerfile -t registry.gitlab.com/oviok-group/poo-process-sandbox:latest --rm=true .
$ docker image push registry.gitlab.com/oviok-group/poo-process-sandbox
```
- Exécutez l'image docker sur la machine locale à partir du registre Gitlab
```sh
# Se connecter et exécuter la commande ci-dessous :
$ docker run -p 8888:8888 registry.gitlab.com/oviok-group/poo-process-sandbox
# Si REST API
$ curl localhost:8888
```
- Suppression du container
```sh
$ docker rm -f $(docker ps -f expose=8888 -q)
```
## 9°) Créer le Pipeline pour CI/CD GitLab
Un pipeline CI/CD est l'ensemble des étapes de bout en bout que le référentiel prend lorsque de nouveaux "commit" sont poussées vers le référentiel.
- Ajout du fichier du fichier de configuration de pipeline: .gitlab-ci.yml
le contenu de notre fichier est le suivant :
```xml
<!-- TODO --> 
```

## 11°) Automatiser la création de l'image Docker avec Maven
Il existe des plugin maven permettant d'automatiser la création d'image Docker à partir de l'application, sans avoir à exécuter des lignes de commandes.

### 11-1°) Le plugin maven exec : 
Il fournit un moyen simple d'exécuter n'importe quel programme dans le cadre d'une construction maven. Il peut donc être utilisé pour créer une image Docker comme nous le ferions à partir de la ligne de commande.
- Avantages :
	- facile 
	- prise en charge complète de Docker
- Inconvenients :
	- pas très efficace avec la superposition de fichiers système Docker 
	- nécessite l'accès à un agent docker pour construire
	- niveaux primitifs d'intégration entre maven et docker
- Exemple de configuration :
dans le pom.xml du projet, sous les balises: <builds><plugins> ..., ajoutez lles lignes suivantes pour une configuration complète :
```xml
<plugin>
	<groupId>org.codehaus.mojo</groupId>
	<artifactId>exec-maven-plugin</artifactId>
	<version>1.6.0</version>
	<executions>
		<!-- Supprimer l'image existante du dépôt local. -->
		<execution>
			<id>docker-clean</id>
			<phase>install</phase>
			<goals>
				<goal>exec</goal>
			</goals>
			<configuration>
				<executable>docker</executable>
				<workingDirectory>${project.basedir}</workingDirectory>
				<arguments>
					<argument>rmi</argument>
					<argument>${project.groupId}/${project.artifactId}:${project.version}
					</argument>
				</arguments>
			</configuration>
		</execution>
		<!-- Créez une nouvelle image docker à l'aide du Dockerfile qui doit être présent dans
		 le répertoire de travail actuel. Mettre un "Tage" (Marquez) l'image à l'aide des informations de version du projet maven
		 ou bien avec "latest". -->
		<execution>
			<id>docker-build</id>
			<phase>install</phase>
			<goals>
				<goal>exec</goal>
			</goals>
			<configuration>
				<executable>docker</executable>
				<workingDirectory>${project.basedir}</workingDirectory>
				<arguments>
					<argument>build</argument>
					<argument>-t</argument>
					<argument>${project.groupId}/${project.artifactId}:${project.version}
					</argument>
					<argument>.</argument>
				</arguments>
			</configuration>
		</execution>
		<!-- Connection et transfert de l'image vers un référentiel Docker. -->
		<execution>
			<id>docker-login</id>
			<phase>deploy</phase>
			<goals>
				<goal>exec</goal>
			</goals>
			<configuration>
				<executable>docker</executable>
				<workingDirectory>${project.basedir}</workingDirectory>
				<arguments>
					<argument>login</argument>
					<argument>-u</argument>
					<argument>${docker.user}</argument>
					<argument>-p</argument>
					<argument>${docker.password}</argument>
					<argument>${docker.url}</argument>
				</arguments>
			</configuration>
		</execution>
		<execution>
			<id>docker-push</id>
			<phase>deploy</phase>
			<goals>
				<goal>exec</goal>
			</goals>
				<configuration>
					<executable>docker</executable>
					<workingDirectory>${project.basedir}</workingDirectory>
					<arguments>
						<argument>push</argument>
						<argument>${project.groupId}/${project.artifactId}:${project.version}
						</argument>
					</arguments>
				</configuration>
		</execution>
	</executions>
</plugin>
```
Exécution avec la configuration :
- Pour compiler et créer l'image Docker localement :
```sh
$ mvn clean install 
```
- Pour compiler, créer et pousser l'image vers un référentiel distant : 
```sh
$ mvn clean deploy -Ddocker.user=<username> -Ddocker.password=<passwd> -Ddocker.url=<docker-registry-url>  
```
### 11-2°) Le plugin maven Docker Spotify : 
Le plugin Spotify Maven fait abstraction de certains concepts de docker et fournit une interface plus traditionnelle de type maven pour configurer une construction de l'image docker. 
Cependant, il utilise toujours un agent docker pour effectuer la construction réelle.
- Avantages :
	- facile 
	- prise en charge plus directe des concepts de docker
	- prend en charge Dockerfile
- Inconvenients :
	- couches du système de fichiers Docker 
	- nécessite l'accès à un agent docker pour construire
- Exemple de configuration :
```
```
	
# TECHNOLOGIES UTILISEES 
- Java : langage de programmation utilisé (version 11)
- Maven : gestion du cycle de vie et construction de projet Java (version 3)
- Spring  : pour bénéficier des ses apports tels que AOP et IOC  
	- AOP : permet de facilement mettre en place des fonctionnalités dans différents points d'une application  
	- IOC : patron de conception qui permet en POO de découpler les dépendances entre objets. Ainsi, il permet de fournir automatiquement aux objets leurs dépendances au lieu de les créér ou de les recevoir en paramètres
- Spring Boot 2+ : facilite la création d'applications autonomes basées sur Spring (avec une configuration Spring minimale) de qualité production que l'on peut facilement «simplement exécuter».
- Jetty comme container de servlets  (environnement d'exécution embarqué) 
- H2 : comme base embarquée pour le démarrage du container 
- Tests : spring-boot-starter-test, spring-test, mockito-all,JUnit, hamcrest-all, assertj-core
- Docker et Docker Compose pour la containerisation
- Kubernetes pour l'orchestrations de conteneurs
- GitLab pour la création des pipelines de CI/CD (Intégration et livraison continues)


#!/bin/sh

while ! nc -z poo-process-sandbox 8888 ; do
    echo "Waiting for upcoming POO SandBox Process"
    sleep 2
done

java -jar /opt/poo-process-sandbox/lib/poo-process-sandbox.jar
/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : PooProcessSandboxApplication.java
 * Date de création : 24 sept. 2020
 * Heure de création : 09:26:20
 * Package : fr.vincent.tuto
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Le starter del'application
 * 
 * @author Vincent Otchoun
 */
@SpringBootApplication
public class PooProcessSandboxApplication
{
    /**
     * @param args
     */
    public static void main(String... args)
    {
        //
        SpringApplication.run(PooProcessSandboxApplication.class, args);

        //
        System.setProperty("javax.xml.bind.JAXBContextFactory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
    }
}

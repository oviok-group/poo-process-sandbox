/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : PooSandboxBaseConfig.java
 * Date de création : 24 sept. 2020
 * Heure de création : 08:35:15
 * Package : fr.vincent.tuto.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Configuration de base du projet
 * 
 * @author Vincent Otchoun
 */
@Configuration
@PropertySource(value = { "classpath:poo-process-sandbox.properties" }, ignoreResourceNotFound = true)
@ComponentScan(basePackages = { "fr.vincent.tuto" })
@ConfigurationProperties(prefix = "vot", ignoreUnknownFields = true, ignoreInvalidFields = false)
// @EntityScan("fr.vincent.tuto.model.po")
// @EnableJpaRepositories(basePackages = "fr.vincent.tuto.repository")
// @ServletComponentScan
public class PooSandboxBaseConfig
{
    //
}

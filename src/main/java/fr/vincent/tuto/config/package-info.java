/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 23 sept. 2020
 * Heure de création : 23:58:20
 * Package : fr.vincent.tuto.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Embarque les différentes configurations du projet
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.config;
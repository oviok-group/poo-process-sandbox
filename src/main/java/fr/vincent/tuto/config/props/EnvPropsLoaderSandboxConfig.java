/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : EnvPropsLoaderSandboxConfig.java
 * Date de création : 24 sept. 2020
 * Heure de création : 09:00:51
 * Package : fr.vincent.tuto.config.props
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.config.props;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.ConfigurableEnvironment;

import fr.vincent.tuto.config.PooSandboxBaseConfig;

/**
 * Configuration de chargement des propriétés externalisées à partir de
 * {@link ConfigurableEnvironment}
 * 
 * @author Vincent Otchoun
 */
@Configuration
// @Import(value = { PooSandboxBaseConfig.class })
@Import(PooSandboxBaseConfig.class)
public class EnvPropsLoaderSandboxConfig
{
    @Autowired
    private ConfigurableEnvironment environment;

    /**
     * Charger les valeurs renseignées pour la clé dans le fichier de propriétés
     * externalisées
     * 
     * @param pKey
     *             la clé definissant la propriété externalisée
     * @return
     */
    public String getExternalProperty(final String pKey)
    {
        return (StringUtils.isNotBlank(pKey)) ? this.environment.getProperty(pKey) : null;
    }

}

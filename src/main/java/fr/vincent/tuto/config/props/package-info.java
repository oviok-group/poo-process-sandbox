/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 24 sept. 2020
 * Heure de création : 08:58:47
 * Package : fr.vincent.tuto.config.props
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contient les composants de chargement et traitement des propriétés
 * externalisées du projet
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.config.props;
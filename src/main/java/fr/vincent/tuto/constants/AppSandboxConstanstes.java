/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : AppSandboxConstanstes.java
 * Date de création : 25 sept. 2020
 * Heure de création : 23:07:21
 * Package : fr.vincent.tuto.constants
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.constants;

/**
 * Utilitaires de mise à disposition des constantes applicatives
 * 
 * @author Vincent Otchoun
 */
public final class AppSandboxConstanstes
{

    public static final String DRIVER_DB = "DRIVER";
    public static final String URL_CONNEXION_DB = "URL";
    public static final String USER_CONNEXION_DB = "USER";
    public static final String PWD_CONNEXION_DB = "USER_PASSWORD";

    /**
     * Constructeur private de l'utilitares
     */
    private AppSandboxConstanstes()
    {
    }

}

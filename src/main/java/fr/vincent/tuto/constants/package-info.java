/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 25 sept. 2020
 * Heure de création : 23:05:52
 * Package : fr.vincent.tuto.constants
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contient les objets fournissant les constantes applicatives
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.constants;
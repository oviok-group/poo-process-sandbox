/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : SandboxBusinessException.java
 * Date de création : 24 sept. 2020
 * Heure de création : 20:57:43
 * Package : fr.vincent.tuto.exceptions
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.exceptions;

/**
 * Classe pour intercepter des exceptions metiers ou d'application. Ces
 * exceptions ne sont pas vérifiees par le compilateur (Unchecked exceptions).
 * Elles sont implicites
 * 
 * @author Vincent Otchoun
 */
public class SandboxBusinessException extends RuntimeException
{
    /**
     * 
     */
    private static final long serialVersionUID = 6791704124197580426L;

    /**
     * Constructeur par defaut pour la creation des instances des objets
     * {@link SandboxBusinessException}.
     */
    public SandboxBusinessException()
    {
        super();
    }

    /**
     * Constructeur avec parametres pour la creation des instances des ovjets
     * {@link SandboxBusinessException}.
     * 
     * @param pMessage
     *                            le message informatif de l'erreur intercepte
     * @param pCause
     *                            la cause de l'erreur
     * @param pEnableSuppression
     *                            flag autorisant la suppresion
     * @param pWritableStackTrace
     *                            flag autorisant l'ecriture de la stack trace
     */
    public SandboxBusinessException(final String pMessage, final Throwable pCause, final boolean pEnableSuppression,
            final boolean pWritableStackTrace)
    {
        super(pMessage, pCause, pEnableSuppression, pWritableStackTrace);
    }

    /**
     * Constructeur avec parametres pour la creation des instances des ovjets
     * {@link SandboxBusinessException}.
     * 
     * @param pMessage
     *                 le message informatif de l'erreur intercepte
     * @param pCause
     *                 la cause de l'erreur
     */
    public SandboxBusinessException(final String pMessage, final Throwable pCause)
    {
        super(pMessage, pCause);
    }

    /**
     * Constructeur avec parametres pour la creation des instances des ovjets
     * {@link SandboxBusinessException}.
     * 
     * @param pMessage
     *                 le message informatif de l'erreur intercepte
     */
    public SandboxBusinessException(final String pMessage)
    {
        super(pMessage);
    }

    /**
     * Constructeur avec parametres pour la creation des instances des ovjets
     * {@link SandboxBusinessException}.
     * 
     * @param pCause
     *               la cause de l'erreur
     */
    public SandboxBusinessException(final Throwable pCause)
    {
        super(pCause);
    }
}

/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : SandboxTechnicalException.java
 * Date de création : 24 sept. 2020
 * Heure de création : 21:20:27
 * Package : fr.vincent.tuto.exceptions
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.exceptions;

/**
 * Classe pour intercepter les exceptions technique ou systeme. Ces exceptions
 * sont vérifiées par le compilateur (Checked exceptions). Elles sont explicites
 * 
 * @author Vincent Otchoun
 */
public class SandboxTechnicalException extends Exception
{
    /**
     * 
     */
    private static final long serialVersionUID = 851301187922236225L;

    /**
     * Constructeur par defaut pour la creation des instances des objets
     * {@link SandboxTechnicalException}.
     */
    public SandboxTechnicalException()
    {
        super();
    }

    /**
     * Constructeur avec parametres pour la creation des instances des ovjets
     * {@link SandboxTechnicalException}.
     * 
     * @param message
     *                           le message informatif de l'erreur intercepte
     * @param cause
     *                           la cause de l'erreur
     * @param enableSuppression
     *                           flag autorisant la suppresion
     * @param writableStackTrace
     *                           flag autorisant l'ecriture de la stack trace
     */
    public SandboxTechnicalException(final String pMessage, final Throwable pCcause, final boolean pEnableSuppression,
            final boolean pWritableStackTrace)
    {
        super(pMessage, pCcause, pEnableSuppression, pWritableStackTrace);
    }

    /**
     * Constructeur avec parametres pour la creation des instances des ovjets
     * {@link SandboxTechnicalException}.
     * 
     * @param pMessage
     *                 le message informatif de l'erreur intercepte
     * @param pCcause
     *                 la cause de l'erreur
     */
    public SandboxTechnicalException(final String pMessage, final Throwable pCcause)
    {
        super(pMessage, pCcause);
    }

    /**
     * Constructeur avec parametres pour la creation des instances des ovjets
     * {@link SandboxTechnicalException}.
     * 
     * @param pMessage
     *                 le message informatif de l'erreur intercepte
     */
    public SandboxTechnicalException(final String pMessage)
    {
        super(pMessage);
    }

    /**
     * Constructeur avec parametres pour la creation des instances des ovjets
     * {@link SandboxTechnicalException}.
     * 
     * @param pCcause
     *                la cause de l'erreur
     */
    public SandboxTechnicalException(final Throwable pCcause)
    {
        super(pCcause);
    }
}

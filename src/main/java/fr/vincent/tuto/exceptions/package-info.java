/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 24 sept. 2020
 * Heure de création : 20:55:06
 * Package : fr.vincent.tuto.exceptions
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contient les composants de gestion des événements indésirables, qui
 * pourraient survenir et empêchant l'exécution normale de l'application.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.exceptions;
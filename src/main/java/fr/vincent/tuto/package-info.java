/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 23 sept. 2020
 * Heure de création : 23:41:09
 * Package : fr.vincent.tuto
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Package de base des éléments du projet
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto;
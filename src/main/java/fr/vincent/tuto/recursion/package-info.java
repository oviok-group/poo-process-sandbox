/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 30 sept. 2020
 * Heure de création : 06:31:36
 * Package : fr.vincent.tuto.recursion
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contient les objets
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.recursion;
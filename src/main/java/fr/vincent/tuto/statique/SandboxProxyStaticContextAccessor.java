/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : SandboxProxyStaticContextAccessor.java
 * Date de création : 24 sept. 2020
 * Heure de création : 19:39:54
 * Package : fr.vincent.tuto.config.statique
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.statique;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;

import com.google.common.collect.Maps;

import fr.vincent.tuto.config.PooSandboxBaseConfig;
import fr.vincent.tuto.exceptions.SandboxBusinessException;

/**
 * Composant permettant d'obtenir un accès statique aux beans avec
 * utilisation de Proxy Java
 * 
 * @author Vincent Otchoun
 */
@Component
@Import(PooSandboxBaseConfig.class)
public class SandboxProxyStaticContextAccessor
{
    //
    private static Logger LOGGER = LoggerFactory.getLogger(SandboxProxyStaticContextAccessor.class);

    @SuppressWarnings("rawtypes")
    private static final Map<Class, SandboxProxyDynamicInvocation> gestionnaireDeClasses = Maps.newHashMap();
    private static ApplicationContext context;

    /**
     * Constructeur avec paramètre du composant de chargemant fournissant l'accès
     * statique aus beans Spring
     */
    @Autowired
    public SandboxProxyStaticContextAccessor(final ApplicationContext pApplicationContext)
    {
        context = pApplicationContext;
    }

    /**
     * Fournit le bean dans le contexte statique à partir de la classe
     * 
     * @param <T>
     *               le type du bean à recupérer
     * @param pClazz
     *               le nom la classe du bean à récupérer
     * @return
     */
    public static <T> T getBean(final Class<T> pClazz)
    {
        if (context == null)
        {
            return getProxy(pClazz);
        }
        return context.getBean(pClazz);
    }

    /**
     * Fournit le bean dans le contexte statique à partir des noms du bean et de la
     * classe
     * 
     * @param <T>
     *                   le type du bean
     * @param pName
     *                   le nom du bean à récupérer
     * @param pClassName
     *                   le nom la classe du bean à récupérer
     * @return
     */
    public static <T> T getBeanWithName(final String pName, final Class<T> pClassName)
    {
        if (context == null)
        {
            return getProxy(pClassName);
        }
        return context.getBean(pName, pClassName);

    }

    /**
     * Retourne l'objet temporaire pour le bean
     * 
     * @param <T>
     *               le type d'objet à retourner
     * @param pClazz
     *               la classe traitée
     * @return
     */
    @SuppressWarnings("unchecked")
    private static <T> T getProxy(final Class<T> pClazz)
    {
        LOGGER.info("[createProxy] - Creation du Proxy");

        //
        try
        {
            final SandboxProxyDynamicInvocation<T> dynamicInvocation = new SandboxProxyDynamicInvocation<>();
            gestionnaireDeClasses.put(pClazz, dynamicInvocation);
            return (T) Proxy.newProxyInstance(pClazz.getClassLoader(), new Class[] { pClazz }, dynamicInvocation);
        } catch (Exception e)
        {
            throw new SandboxBusinessException(e);
        }
    }

    /**
     * Utiliser le contexte pour obtenir les beans réels et les transmettre aux
     * gestionnaires d'appel
     */
    @SuppressWarnings("unchecked")
    @PostConstruct
    private void init()
    {
        LOGGER.info("[init] - Initialisation éléments du contexte statique");
        gestionnaireDeClasses.forEach((myClass, invocationHandler) -> {
            final Object realBean = context.getBean(myClass);
            invocationHandler.setActualBean(realBean);
        });
    }

    /**
     * Classe d'invocation dynamique d'une méthode sur le Proxy
     * 
     * @author Vincent Otchoun
     * @param <T>
     *            le type del'objet traité
     */
    static class SandboxProxyDynamicInvocation<T> implements InvocationHandler
    {
        //
        private T monBean;

        /**
         * @param pActualBean
         */
        public void setActualBean(T pActualBean)
        {
            this.monBean = pActualBean;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable
        {
            // lever une exception metier lorsque le bean est null
            this.checkBeanNull();
            return method.invoke(this.monBean, args);
        }

        /**
         * 
         */
        private void checkBeanNull()
        {
            if (this.monBean == null)
            {
                throw new SandboxBusinessException(
                        "Le bean n'est pas encore initialisé pour l'invocation dynamique ! :(");

                // RuntimeException("Le bean n'est pas encore initialisé pour l'invocation
                // dynamique! :(");
            }
        }
    }
}

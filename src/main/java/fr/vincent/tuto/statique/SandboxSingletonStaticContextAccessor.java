/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : SandboxSingletonStaticContextAccessor.java
 * Date de création : 25 sept. 2020
 * Heure de création : 13:46:52
 * Package : fr.vincent.tuto.statique
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.statique;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import fr.vincent.tuto.config.PooSandboxBaseConfig;

/**
 * Composant de mise en place de l'accès statique aux bean en mode Singleton
 * 
 * @author Vincent Otchoun
 */
@Component
public class SandboxSingletonStaticContextAccessor
{
    private static final Logger LOGGER = LoggerFactory.getLogger(SandboxSingletonStaticContextAccessor.class);

    private static SandboxSingletonStaticContextAccessor instance;

    @Autowired
    private ApplicationContext applicationContext;

    @PostConstruct
    public void registerInstance()
    {
        instance = this;
    }

    ///////////////////////////
    // METHODES PUBLIQUES
    //////////////////////////

    /**
     * Fournit le bean dans le contexte statique à partir de la classe
     * 
     * @param <T>        le type du bean à recupérer
     * @param pClassName le nom la classe du bean à récupérer
     * @return
     * @throws BeansException exception levée lorsque survient une erreur
     */
    public static <T> T getBean(final Class<T> pClassName) throws BeansException
    {
        initContext();
        return instance.applicationContext.getBean(pClassName);
    }

    /**
     * @param <T>        le type du bean à recupére
     * @param pName      le nom du bean à récupérer
     * @param pClassName le nom la classe du bean à récupérer
     * @return
     * @throws BeansException exception levée lorsque survient une erreur
     */
    public static <T> T getBean(final String pName, final Class<T> pClassName) throws BeansException
    {
        initContext();
        return instance.applicationContext.getBean(pName, pClassName);
    }

    /**
     * Founit la fabrique de bean pour accéder aux composants applications
     * 
     * @return
     * @throws IllegalStateException exception levée lorsque survient une erreur
     */
    public static AutowireCapableBeanFactory getBeanFactory() throws IllegalStateException
    {
        initContext();
        return instance.applicationContext.getAutowireCapableBeanFactory();
    }

    ///////////////////////////
    // METHODES PRIVEES
    //////////////////////////
    /**
     * Constructeur private de la classe
     * 
     * @param pApplicationContext
     *                            le contexte applicatif
     */
    private SandboxSingletonStaticContextAccessor(final ApplicationContext pApplicationContext)
    {
        this.applicationContext = pApplicationContext;
    }

    /**
     * Initialisation du contexte applicatif pour le chargement de l'instance
     * unique
     */
    private static synchronized void initContext()
    {
        LOGGER.info("[initContext] - Initialisation du contexte Spring ...");
        if (instance == null)
        {
            final ApplicationContext context = new AnnotationConfigApplicationContext(PooSandboxBaseConfig.class);
            instance = new SandboxSingletonStaticContextAccessor(context);
        }
    }
}

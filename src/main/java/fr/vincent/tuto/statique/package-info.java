/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 24 sept. 2020
 * Heure de création : 20:05:11
 * Package : fr.vincent.tuto.statique
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contient les composants permettant d'obtenir un accès statique aux beans
 * Spring à partir du contexte d'application
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.statique;
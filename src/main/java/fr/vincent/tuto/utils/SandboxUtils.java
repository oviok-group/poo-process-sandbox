/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : SandboxUtils.java
 * Date de création : 24 sept. 2020
 * Heure de création : 00:27:38
 * Package : fr.vincent.tuto.utils
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.utils;

import java.time.Instant;
import java.util.Date;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.vincent.tuto.exceptions.SandboxBusinessException;

/**
 * @author Vincent Otchoun
 */
public final class SandboxUtils
{
    //
    private static final Logger LOGGER = LoggerFactory.getLogger(SandboxUtils.class);

    private static final String TABLEAU_ILLEGAL = "Le tableau ne devrait pas être null";
    private static final String NOM_METHOD_ILLEGAL = "Le nom de méthode ne devrait pas être null ou vide";
    private static final String DUREE_ILLEGAL = "La durée d'exécution devrait être > 0";
    private static final String MESSAGE_COMPARER = "[comparerDuree] - La méthode : {} est plus rapide de : {} milliseconds ({} % plus rapide )";

    /**
     * Constructeur private de l'utilitaire
     */
    private SandboxUtils()
    {
        //
    }

    /**
     * Calculer la durée entre la valeur en paramètre et le temps du système lors
     * de l'appel de la méthode.
     * <ul>
     * <li>le paramètre d'entrée est fourni en nano secondes avec :
     * System.nanoTime()</li>
     * <li>le retour est en millisecondes</li>
     * </ul>
     * 
     * @param pInitialTime la durée intiale en nano secondes
     * @return
     */
    public static long calculerDureeExecution(final long pInitialTime)
    {
        LOGGER.info("[calculerDureeExecution] - Durée d'exécution de la méthode en millisecondes");
        return calculerDureeExecutionNanoSecondes(pInitialTime) / 1000000;
    }

    /**
     * Calculer la duréee d'exécution avec {@link Instant}.
     * <ul>
     * <li>le paramètre d'entrée est fourni millisecondes avec :
     * Instant.now().toEpochMilli()</li>
     * <li>le retour est en millisecondes</li>
     * </ul>
     * 
     * @param pInitialTime la durée intiale en millisecondes
     * @return
     */
    public static long calculerDureeWithInstant(final long pInitialTime)
    {
        LOGGER.info("[calculerDureeWithInstant] - Durée d'exécution de la méthode en millisecondes avec Instant");
        return Instant.now().toEpochMilli() - pInitialTime;
    }

    /**
     * Calculer la duréee d'exécution avec la {@link Date}
     * <ul>
     * <li>le paramètre d'entrée est fourni millisecondes avec :
     * new Date().getTime()</li>
     * <li>le retour est en millisecondes</li>
     * </ul>
     * 
     * @param pInitialTime la durée intiale en millisecondes
     * @return
     */
    public static long calculerDureeWithDate(final long pInitialTime)
    {
        LOGGER.info("[calculerDureeWithDate] - Durée d'exécution de la méthode en millisecondes avec Instant");
        return new Date().getTime() - pInitialTime;
    }

    /**
     * Comparer les temps d'excécution de méthodes
     * 
     * @param pDureeA      la durée d'exécution de la 1ère méthode
     * @param pDureeB      la durée d'exécution de la 2ème méthode
     * @param pNomMethodeA le nom de la 1ère méthode
     * @param pNomMethodeB le nom de la 2ème méthode
     * @return
     */
    public static long comparerDuree(final long pDureeA, final long pDureeB, final String pNomMethodeA,
            final String pNomMethodeB)
    {
        LOGGER.info("[comparerDuree] - Comparer le temps d'exécution entre méthodes");

        // check validité des noms des méthodes
        if (StringUtils.isBlank(pNomMethodeA) || StringUtils.isBlank(pNomMethodeB))
        {
            throw new SandboxBusinessException(NOM_METHOD_ILLEGAL);
        }

        // check validité des durées d'exécution des méthodes
        if (pDureeA < 0 || pDureeB < 0)
        {
            throw new SandboxBusinessException(DUREE_ILLEGAL);
        }

        /*
         * Taux d’évolution/taux de variation (= pourcentage d’évolution), la formule
         * est la suivante :
         * ((y2 - y1) / y1)*100 = votre taux d' évolution
         * (où y1=première valeur et y2=deuxième valeur)
         */

        long difference = -1l;
        double pourcentage = -1d;
        String plusRapide = null;

        // calcul du temps d'exécution/ méthode plus rapide/ pourcentage
        if (pDureeA > pDureeB)
        {
            difference = pDureeA - pDureeB;
            plusRapide = pNomMethodeB;
            pourcentage = 100d - (difference + 100d / pDureeB);
            // pourcentage = 100d - ((difference / pDureeB) * 100d);

        } else
        {
            difference = pDureeB - pDureeA;
            plusRapide = pNomMethodeA;
            pourcentage = 100d - (difference + 100d / pDureeA);
            // pourcentage = 100d - ((difference / pDureeA) * 100d);
        }

        LOGGER.info(MESSAGE_COMPARER, plusRapide, difference, pourcentage);

        return difference;
    }

    /**
     * Retourner le nombre de lignes ou de colonnes d'un tableau d'entiers à deux
     * dimensions
     * 
     * @param pTabEntiers    le tableau d'entiers
     * @param pFlagDimension si true retourne le nombre de lignes si false retourne
     *                       le nombre de colonnes
     * @return
     */
    public static int calculerDimensionTableauEntiers(final int[][] pTabEntiers, final Boolean pFlagDimension)
    {
        LOGGER.info("[calculerDimensionTableauEntiers] - Nombre de lignes/colonnes du tableau");

        if (isTableauEntiersNull(pTabEntiers))
        {
            throw new SandboxBusinessException(TABLEAU_ILLEGAL);
        }

        //
        final int nbLignes = pTabEntiers.length;
        final int nbColonnes = pTabEntiers[0].length;
        return BooleanUtils.isTrue(pFlagDimension) ? nbLignes : nbColonnes;
    }

    /**
     * Calculer la durée entre la valeur en paramètre et le temps du système lors de
     * l'appel de la méthode
     * 
     * @param pInitialTime la durée intiale en nano secondes
     * @return
     */
    private static long calculerDureeExecutionNanoSecondes(final long pInitialTime)
    {
        LOGGER.debug("[calculerDureeExecutionNanoSecondes] - Durée d'exécution de la méthode en nano secondes");

        // final long duree = System.currentTimeMillis() - pInitialTime;
        return System.nanoTime() - pInitialTime;
    }

    /**
     * Permet de vérifier que le tableau est null
     * 
     * @param pTabEntiers le tabeau des entiers
     * @return
     */
    private static boolean isTableauEntiersNull(final int[][] pTabEntiers)
    {
        return pTabEntiers == null || pTabEntiers.length < 0;
    }
}

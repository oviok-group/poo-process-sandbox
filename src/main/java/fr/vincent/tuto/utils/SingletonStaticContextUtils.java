/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : SingletonStaticContextUtils.java
 * Date de création : 25 sept. 2020
 * Heure de création : 23:04:47
 * Package : fr.vincent.tuto.utils
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.utils;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.google.common.collect.Maps;

import fr.vincent.tuto.config.props.EnvPropsLoaderSandboxConfig;
import fr.vincent.tuto.constants.AppSandboxConstanstes;
import fr.vincent.tuto.statique.SandboxSingletonStaticContextAccessor;

/**
 * Composant de mise en place de l'accès statique aux bean avec le mode
 * Singleton
 * 
 * @author Vincent Otchoun
 */
@Component
public class SingletonStaticContextUtils
{
    //
    private static final Logger LOGGER = LoggerFactory.getLogger(ProxyStaticContextUtils.class);

    private static Map<String, String> dictionnaireProps = Maps.newHashMap();

    /**
     * Récuperer le bean de chargment des propriétés pour le stockage des
     * informations de la datasource dans un cache
     * 
     * @return
     */
    public static Map<String, String> getProps()
    {
        LOGGER.info("[getProps] - Chargement des propriétés dans un contexte statique ");

        // Vider le cache avant de commencer
        dictionnaireProps.clear();

        // Charger le bean dans le contexte statique avec le nom de la classe
        final EnvPropsLoaderSandboxConfig envPropsLoaderSandboxConfig = SandboxSingletonStaticContextAccessor
                .getBean(EnvPropsLoaderSandboxConfig.class);

        // Récupéraion des propriétés et stockage
        final String driver = envPropsLoaderSandboxConfig.getExternalProperty("vot.datasource.driver");
        final String url = envPropsLoaderSandboxConfig.getExternalProperty("vot.datasource.url");
        final String user = envPropsLoaderSandboxConfig.getExternalProperty("vot.datasource.user");
        final String paswword = envPropsLoaderSandboxConfig.getExternalProperty("vot.datasource.password");

        dictionnaireProps.put(AppSandboxConstanstes.DRIVER_DB, driver);
        dictionnaireProps.put(AppSandboxConstanstes.URL_CONNEXION_DB, url);
        dictionnaireProps.put(AppSandboxConstanstes.USER_CONNEXION_DB, user);
        dictionnaireProps.put(AppSandboxConstanstes.PWD_CONNEXION_DB, paswword);

        return dictionnaireProps;
    }

    public static Map<String, String> getPropsWithBeanName()
    {
        LOGGER.info("[getPropsWithBeanName] - Chargement des propriétés dans un contexte statique avec non du bean ");

        // Vider le cache avant de commencer
        dictionnaireProps.clear();

        // Charger le bean dans le contexte statique avec le nom de la classe
        final EnvPropsLoaderSandboxConfig envPropsLoaderSandboxConfig = SandboxSingletonStaticContextAccessor
                .getBean("envPropsLoaderSandboxConfig", EnvPropsLoaderSandboxConfig.class);

        // Récupéraion des propriétés et stockage
        final String driver = envPropsLoaderSandboxConfig.getExternalProperty("vot.datasource.driver");
        final String url = envPropsLoaderSandboxConfig.getExternalProperty("vot.datasource.url");
        final String user = envPropsLoaderSandboxConfig.getExternalProperty("vot.datasource.user");
        final String paswword = envPropsLoaderSandboxConfig.getExternalProperty("vot.datasource.password");

        dictionnaireProps.put(AppSandboxConstanstes.DRIVER_DB, driver);
        dictionnaireProps.put(AppSandboxConstanstes.URL_CONNEXION_DB, url);
        dictionnaireProps.put(AppSandboxConstanstes.USER_CONNEXION_DB, user);
        dictionnaireProps.put(AppSandboxConstanstes.PWD_CONNEXION_DB, paswword);

        return dictionnaireProps;
    }

    /**
     * @return the dictionnaireProps
     */
    public static Map<String, String> getDictionnaireProps()
    {
        return dictionnaireProps;
    }

}

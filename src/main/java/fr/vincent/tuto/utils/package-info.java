/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : package-info.java
 * Date de création : 24 sept. 2020
 * Heure de création : 00:05:11
 * Package : fr.vincent.tuto.utils
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contient les utilitaires du projet
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.utils;
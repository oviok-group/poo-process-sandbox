/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : EnvPropsLoaderSandboxConfigTest.java
 * Date de création : 24 sept. 2020
 * Heure de création : 09:15:49
 * Package : fr.vincent.tuto.config.props
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.config.props;

import static org.assertj.core.api.Assertions.assertThat;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.vincent.tuto.config.PooSandboxBaseConfig;

/**
 * Classe des tests unitaires des objets de type
 * {@link EnvPropsLoaderSandboxConfig}
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = "classpath:poo-process-sandbox-test.properties")
@ContextConfiguration(name = "envPropsLoaderSandboxConfigTest", classes = { PooSandboxBaseConfig.class,
        EnvPropsLoaderSandboxConfig.class })
@SpringBootTest
@ActiveProfiles("test")
public class EnvPropsLoaderSandboxConfigTest
{
    @Autowired
    private EnvPropsLoaderSandboxConfig propsLoader;

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.propsLoader = null;
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.config.props.EnvPropsLoaderSandboxConfig#getExternalProperty(java.lang.String)}.
     */
    @Test
    public void testGetExternalProperty()
    {
        // DataSource props
        final String driver = this.propsLoader.getExternalProperty("vot.datasource.driver");
        final String url = this.propsLoader.getExternalProperty("vot.datasource.url");
        final String user = this.propsLoader.getExternalProperty("vot.datasource.user");
        final String paswword = this.propsLoader.getExternalProperty("vot.datasource.password");

        // PU props
        final String persistanceUnitName = this.propsLoader.getExternalProperty("vot.datasource.unit-name");
        final String databaseName = this.propsLoader.getExternalProperty("vot.datasource.database-name");
        final String dialect = this.propsLoader.getExternalProperty("vot.datasource.dialect");
        final String packageToScan = this.propsLoader.getExternalProperty("vot.datasource.package-to-scan");

        // DDL-SQL props
        final String ddlAuto = this.propsLoader.getExternalProperty("vot.datasource.hbm2ddl-auto");
        final String generatDDL = this.propsLoader.getExternalProperty("vot.datasource.generate-ddl");
        final String showSQL = this.propsLoader.getExternalProperty("vot.datasource.show-sql");
        final String formatSQL = this.propsLoader.getExternalProperty("vot.datasource.format-sql");
        final String commentSQL = this.propsLoader.getExternalProperty("vot.datasource.comment-sql");

        // Hibernate props
        final String hibernateLazyEnable = this.propsLoader.getExternalProperty("vot.datasource.hibernate.enable.lazy");
        final String secondLevelCache = this.propsLoader
                .getExternalProperty("vot.jpa.hibernate.use-second-level-cache");
        final String generateStatistics = this.propsLoader.getExternalProperty("vot.jpa.hibernate.generate-statistics");
        final String reflexionOptimizer = this.propsLoader
                .getExternalProperty("vot.jpa.hibernate.use-reflection-optimizer");

        //
        assertThat(driver).isEqualTo("org.h2.Driver");
        assertThat(url).isEqualTo("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE");
        assertThat(user).isEqualTo("test");
        assertThat(paswword).isEqualTo("test");

        assertThat(persistanceUnitName).isEqualTo("poo-sandbox-pu-test");
        assertThat(databaseName).isEqualTo("H2");
        assertThat(dialect).isEqualTo("org.hibernate.dialect.MySQL5Dialect");
        assertThat(packageToScan).isEqualTo("fr.vincent.tuto");

        assertThat(ddlAuto).isEqualTo("create-drop");
        assertThat(Boolean.valueOf(generatDDL)).isTrue();
        assertThat(Boolean.valueOf(showSQL)).isTrue();
        assertThat(Boolean.valueOf(formatSQL)).isTrue();
        assertThat(Boolean.valueOf(commentSQL)).isTrue();

        assertThat(Boolean.valueOf(hibernateLazyEnable)).isTrue();
        assertThat(Boolean.valueOf(secondLevelCache)).isTrue();
        assertThat(Boolean.valueOf(generateStatistics)).isTrue();
        assertThat(Boolean.valueOf(reflexionOptimizer)).isTrue();
    }

    @Test
    public void testGetExternalPropertyWithNull()
    {
        //
        final String props = this.propsLoader.getExternalProperty(null);

        assertThat(props).isNull();
    }

    @Test
    public void testGetExternalPropertyWithEmpty()
    {
        //
        final String props = this.propsLoader.getExternalProperty(StringUtils.EMPTY);

        assertThat(props).isNull();
    }

    @Test
    public void testNotNullResource()
    {
        assertThat(this.propsLoader).isNotNull();
    }
}

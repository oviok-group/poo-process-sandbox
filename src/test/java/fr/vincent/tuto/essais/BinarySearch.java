/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : BinarySearch.java
 * Date de création : 9 oct. 2020
 * Heure de création : 10:21:16
 * Package : fr.vincent.tuto.utils
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.essais;

/**
 * @author Vincent Otchoun
 */
public class BinarySearch
{
    /*
     * tab[] : le tableau dans lequel on va chercher la valeur
     * l : dernier élément
     * f : premier élément
     * val : valeur à trouver
     */
    public static void binarySearch(int tab[], int f, int l, int val)
    {
        int mid = (f + l) / 2;
        while (f <= l)
        {
            if (tab[mid] < val)
            {
                f = mid + 1;
            } else if (tab[mid] == val)
            {
                System.out.println("L'élément se trouve à l'index: " + mid);
                break;
            } else
            {
                l = mid - 1;
            }
            mid = (f + l) / 2;
        }
        if (f > l)
        {
            System.out.println("L'élément n'existe pas!");
        }
    }

    public static boolean binarySearchRecursive(int[] tab, int val)
    {
        int iDebut = 0;
        int iFin = tab.length - 1;
        if (iDebut <= iFin)
        {
            int milieu = (iDebut + iFin) / 2;

            if (tab[milieu] > val)
            {

            } else
            {

            }

        }

        return false;

    }

    /*
     * tab[] : le tableau dans lequel on va chercher la valeur
     * l : dernier élément
     * f : premier élément
     * val : valeur à trouver
     */
    public static int binarySearchRecursive(int tab[], int f, int l, int val)
    {
        if (l >= f)
        {
            int mid = f + (l - f) / 2;
            if (tab[mid] == val)
            {
                return mid;
            }
            if (tab[mid] > val)
            {
                // recherche dans le sous-tableau à gauche
                return binarySearchRecursive(tab, f, mid - 1, val);
            } else
            {
                // recherche dans le sous-tableau à droit
                return binarySearchRecursive(tab, mid + 1, l, val);
            }
        }
        return -1;
    }

    /**
     * @param args
     */
    public static void main(String args[])
    {
        // Non recursive
        int tab[] = { 1, 2, 3, 4, 5, 6, 7 };
        int val = 4;
        int l = tab.length - 1;
        binarySearch(tab, 0, l, val);

        //
        int tab1[] = { 1, 2, 3, 4, 5, 6, 7 };
        int val1 = 4;
        int l1 = tab1.length - 1;
        int res = binarySearchRecursive(tab1, 0, l1, val1);
        if (res != -1)
            System.out.println("L'élément se trouve à l'index: " + res);
        else
            System.out.println("L'élément n'existe pas!");
    }

}

/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : LetterH.java
 * Date de création : 4 oct. 2020
 * Heure de création : 12:11:26
 * Package : fr.vincent.tuto
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.essais;

/**
 * @author Vincent Otchoun
 */
public class LetterH
{

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        LetterH h = new LetterH();
        h.toString();

        System.err.println(3 + "4");
        System.err.println("2°) " + "3" + "4");
        System.err.println(3 + 4);

    }

    public String toString()
    {
        // System.err.println("* *\n* *\n*****\n* *\n* *\n");
        // System.err.println("*****\n*\n*****\n*\n*****\n");
        String ligne1 = "*   * ***** *     *       *  ";
        String ligne2 = "*   * *     *     *     *    *";
        String ligne3 = "***** ***** *     *     *    *";
        String ligne4 = "*   * *     *     *     *    *";
        String ligne5 = "*   * ***** ***** *****   *  ";

        //
        System.err.println(ligne1 + "\n" + ligne2 + "\n" + ligne3 + "\n" + ligne4 + "\n" + ligne5 + "\n");

        System.out.println("Ligne1 =" + ligne1.length());
        System.out.println("Ligne2 =" + ligne2.length());
        System.out.println("Ligne3 =" + ligne3.length());
        System.out.println("Ligne4 =" + ligne4.length());
        System.out.println("Ligne5 =" + ligne5.length());

        //
        // System.err.println(
        // "* * ***** * * *\n* * * * * * *\n***** ***** * * * *\n* * * * * * *\n* *
        // ***** ***** ***** *\n");
        return "* *\n* *\n*****\n* *\n* *\n";
    }

}

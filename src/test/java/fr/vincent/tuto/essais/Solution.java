/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : Solution.java
 * Date de création : 8 oct. 2020
 * Heure de création : 20:15:24
 * Package : fr.vincent.tuto.utils
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.essais;

import java.util.Scanner;

/**
 * @author Vincent Otchoun
 */
public class Solution
{

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt(); // the number of temperatures to analyse

        int temp = 0;
        int min = Integer.MAX_VALUE;
        int[] memoire = new int[n];

        for (int i = 0; i < n; i++)
        {
            int t = in.nextInt(); // a temperature expressed as an integer ranging from -273 to 5526

            // Stockage des nombres lus
            memoire[i] = t;

            // -15 -7 -9 12 -12
            // -15 -7 -9 7 -12
            //
            if (Math.abs(t) < min)
            {
                min = Math.abs(t);
                temp = t;
            } else if ((Math.abs(t) == Math.abs(min)) && t > 0)
            {
                temp = Math.max(t, min);
            }
        }

        // Write an answer using System.out.println()
        // To debug: System.err.println("Debug messages...");
        System.out.println(temp);
    }

}

/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : Test.java
 * Date de création : 9 oct. 2020
 * Heure de création : 11:11:21
 * Package : fr.vincent.tuto.utils
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.essais;

import java.util.Arrays;

/**
 * @author Vincent Otchoun
 */
public class Test
{

    private static final int TAILLE_MAX = 1000000;
    // private static int[] ints = new int[TAILLE_MAX];

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        // int[] ints = { -9, 14, 37, 102 };
        // int[] ints = { -9, 14, 37, 102, 105, 105, 108, 110, 200, 300, 400, 500, 600, 700 };
        // int[] ints = {};
        int[] ints = null;

        // int[] ints = new int[TAILLE_MAX];
        // initTab(ints);
        System.out.println(Test.exists(ints, 102));
        System.out.println(Test.exists(ints, 36));
    }


    static void initTab(final int[] tab)
    {
        for (int i = 0; i < TAILLE_MAX; i++)
        {
            tab[i] = i;
        }
    }

    static boolean exists(final int[] ints, final int k)
    {
        // if (ints != null && ints.length != 0)
        if (ints != null)
        {
            // System.out.println(0);
            // trie le tableau pour pouvoir faire une recherche dichotomique
            // Arrays.sort(ints);

            // position dans le tableau de la valeur recherchée
            // final int position = Arrays.binarySearch(ints, k);

            // if (position >= 0)
            // {
            // return true;
            // }

            return Arrays.binarySearch(ints, k) >= 0;

            // else
            // {
            // return false;
            // }
            // return;
        }
        // // trie le tableau pour pouvoir faire une recherche dichotomique
        // Arrays.sort(ints);
        //
        // // position dans le tableau de la valeur recherchée
        // final int position = Arrays.binarySearch(ints, k);
        //
        // if (position >= 0)
        // {
        // return true;
        // } else
        // {
        // return false;
        // }
        return false;
    }

    /*
     * tab[] : le tableau dans lequel on va chercher la valeur
     * l : dernier élément
     * f : premier élément
     * val : valeur à trouver
     */
    public static int binarySearchRecursive(int tab[], int f, int l, int val)
    {
        if (l >= f)
        {
            int mid = f + (l - f) / 2;
            if (tab[mid] == val)
            {
                return mid;
            }
            if (tab[mid] > val)
            {
                // recherche dans le sous-tableau à gauche
                return binarySearchRecursive(tab, f, mid - 1, val);
            }
            else
            {
                // recherche dans le sous-tableau à droit
                return binarySearchRecursive(tab, mid + 1, l, val);
            }
        }
        return -1;
    }

}

package fr.vincent.tuto.essais;

import java.util.Scanner;

/**
 * Don't let the machines win. You are humanity's last hope...
 **/
public class ThereIsNoSpoonVot
{
    private static final char ZERO = '0';
    private static final String ESPACE = " ";
    private static final String NO_NEIGBOUR = "-1 -1 ";

    /**
     * @param args
     */
    public static void main(String args[])
    {
        Scanner in = new Scanner(System.in);
        int width = in.nextInt(); // the number of cells on the X axis
        int height = in.nextInt(); // the number of cells on the Y axis

        if (in.hasNextLine())
        {
            in.nextLine();
        }

        /**
         * Traitement global pour dimension 2
         */
        if (width > 0 && height > 0)
        {
            char[][] rectangle = new char[height][width];

            // Intialisation de la grille à deux dimensions
            initialiserGrille(in, height, rectangle);

            //
            afficherGrille(width, height, rectangle);

            // Recherche voisins sur deux dimensions
            rechercherVoisins(width, height, rectangle);
        }

        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");

        // Three coordinates: a node, its right neighbor, its bottom neighbor
    }

    /**
     * @param width
     * @param height
     * @param rectangle
     */
    private static void rechercherVoisins(final int width, final int height, final char[][] rectangle)
    {
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                if (rectangle[i][j] == ZERO)
                {
                    String result = "" + (j) + ESPACE + (i) + ESPACE;
                    int i1 = i + 1;
                    int j1 = j + 1;
                    while (i1 < height)
                    {
                        if (rectangle[i1][j] == ZERO)
                        {
                            break;
                        }
                        i1++;
                    }
                    while (j1 < width)
                    {
                        if (rectangle[i][j1] == ZERO)
                        {
                            break;
                        }
                        j1++;
                    }
                    if (j1 >= width)
                    {
                        result = result + NO_NEIGBOUR;
                    } else
                    {
                        result = result + (j1) + ESPACE + (i) + ESPACE;
                    }
                    if (i1 >= height)
                    {
                        result = result + "-1 -1";
                    } else
                    {
                        result = result + (j) + ESPACE + (i1) + "";
                    }

                    System.out.println(result);
                }
            }
        }
    }

    /**
     * @param width
     * @param height
     * @param rectangle
     */
    private static void afficherGrille(final int width, final int height, final char[][] rectangle)
    {
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                System.err.print(rectangle[i][j] + ESPACE);
            }
            System.err.println();
        }
        System.err.println(width + ESPACE + height);
    }

    /**
     * @param in
     * @param height
     * @param rectangle
     */
    private static void initialiserGrille(final Scanner in, final int height, final char[][] rectangle)
    {
        for (int i = 0; i < height; i++)
        {
            String line = in.nextLine(); // width characters, each either 0 or .
            for (int j = 0; j < line.length(); j++)
            {
                rectangle[i][j] = line.charAt(j);
            }
        }
    }
}
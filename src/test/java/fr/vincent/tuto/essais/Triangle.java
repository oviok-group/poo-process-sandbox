/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : Triangle.java
 * Date de création : 4 oct. 2020
 * Heure de création : 12:53:29
 * Package : fr.vincent.tuto
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.essais;

/**
 * This class describes triangle objects that can be displayed
 * as shapes like this:
 * []
 * [][]
 * [][][].
 * 
 * @author Vincent Otchoun
 */
public class Triangle
{
    //
    private int width;

    /**
     * Constructs a triangle.
     * 
     * @param pWidth the number of [] in the last row of the triangle
     */
    private Triangle(int pWidth)
    {
        super();
        this.width = pWidth;
    }

    /**
     * Computes a string representing the triangle.
     * 
     * @return a string consisting of [] and newline characters
     */

    public String toString()
    {
        String r = "";
        for (int i = 1; i <= width; i++)
        {
            // Make triangle row
            for (int j = 1; j <= i; j++)
                r = r + "[]";
            r = r + "\n";
        }
        return r;
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        Triangle small = new Triangle(3);
        System.out.println(small.toString());

        Triangle large = new Triangle(15);
        System.out.println(large.toString());

    }

}

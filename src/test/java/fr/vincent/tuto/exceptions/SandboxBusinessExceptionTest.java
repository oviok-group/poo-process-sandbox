/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : SandboxBusinessExceptionTest.java
 * Date de création : 24 sept. 2020
 * Heure de création : 21:03:26
 * Package : fr.vincent.tuto.exceptions
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.exceptions;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Classe des tests unitaires des objets de type
 * {@link SandboxBusinessException}
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(name = "sandboxBusinessExceptionTest", classes = { SandboxBusinessException.class })
@SpringBootTest
public class SandboxBusinessExceptionTest
{

    private SandboxBusinessException businessException;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        this.businessException = new SandboxBusinessException();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.businessException = null;
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.exceptions.SandboxBusinessException#SandboxBusinessException()}.
     */
    @Test
    public void testSandboxBusinessException()
    {
        //
        assertThat(this.businessException.getCause()).isNull();
        assertThat(this.businessException.getMessage()).isNull();
        assertThat(this.businessException.getLocalizedMessage()).isNull();
        assertThat(this.businessException.getStackTrace()).isNotNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.exceptions.SandboxBusinessException#SandboxBusinessException(java.lang.String)}.
     */
    @Test
    public void testSandboxBusinessExceptionString()
    {
        final String message = "Message Erreur Exception";
        this.businessException = new SandboxBusinessException(message);

        //
        assertThat(this.businessException.getCause()).isNull();
        assertThat(this.businessException.getMessage()).isEqualTo(message);
        assertThat(this.businessException.getLocalizedMessage()).isEqualTo(message);
        assertThat(this.businessException.getStackTrace()).isNotNull();
    }

    @Test
    public void testSandboxBusinessExceptionStringWithNull()
    {
        this.businessException = new SandboxBusinessException((String) null);

        //
        assertThat(this.businessException.getCause()).isNull();
        assertThat(this.businessException.getMessage()).isNull();
        assertThat(this.businessException.getLocalizedMessage()).isNull();
        assertThat(this.businessException.getStackTrace()).isNotNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.exceptions.SandboxBusinessException#SandboxBusinessException(java.lang.Throwable)}.
     */
    @Test
    public void testSandboxBusinessExceptionThrowable()
    {
        final String message = "Message Erreur Exception";
        final Throwable throwable = new Throwable(message);
        this.businessException = new SandboxBusinessException(throwable);

        //
        assertThat(this.businessException.getCause()).isNotNull();
        assertThat(this.businessException.getCause().getMessage()).isEqualTo(throwable.getMessage());
        assertThat(this.businessException.getStackTrace()).isNotNull();
    }

    @Test
    public void testSandboxBusinessExceptionThrowableWithNull()
    {
        this.businessException = new SandboxBusinessException((Throwable) null);

        //
        assertThat(this.businessException.getCause()).isNull();
        assertThat(this.businessException.getMessage()).isNull();
        assertThat(this.businessException.getLocalizedMessage()).isNull();
        assertThat(this.businessException.getStackTrace()).isNotNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.exceptions.SandboxBusinessException#SandboxBusinessException(java.lang.String, java.lang.Throwable)}.
     */
    @Test
    public void testSandboxBusinessExceptionStringThrowable()
    {
        final String message = "Message Erreur Exception";
        final Throwable throwable = new Throwable(message);
        this.businessException = new SandboxBusinessException(message, throwable);

        //
        assertThat(this.businessException.getCause()).isNotNull();
        assertThat(this.businessException.getMessage()).isNotNull();
        assertThat(this.businessException.getLocalizedMessage()).isNotNull();
        assertThat(this.businessException.getStackTrace()).isNotNull();
    }

    @Test
    public void testSandboxBusinessExceptionStringThrowableWithNull()
    {
        this.businessException = new SandboxBusinessException(null, null);

        //
        assertThat(this.businessException.getCause()).isNull();
        assertThat(this.businessException.getMessage()).isNull();
        assertThat(this.businessException.getLocalizedMessage()).isNull();
        assertThat(this.businessException.getStackTrace()).isNotNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.exceptions.SandboxBusinessException#SandboxBusinessException(java.lang.String, java.lang.Throwable, boolean, boolean)}.
     */
    @Test
    public void testSandboxBusinessExceptionStringThrowableBooleanBoolean()
    {
        final String message = "Message Erreur Exception";
        final Throwable throwable = new Throwable(message);
        this.businessException = new SandboxBusinessException(message, throwable, true, true);

        //
        assertThat(this.businessException.getCause()).isNotNull();
        assertThat(this.businessException.getMessage()).isNotNull();
        assertThat(this.businessException.getLocalizedMessage()).isNotNull();
        assertThat(this.businessException.getStackTrace()).isNotNull();
    }

    @Test
    public void testSandboxBusinessExceptionStringThrowableBooleanBooleanFalse()
    {
        final String message = "Message Erreur Exception";
        final Throwable throwable = new Throwable(message);
        this.businessException = new SandboxBusinessException(message, throwable, false, false);

        //
        assertThat(this.businessException.getCause()).isNotNull();
        assertThat(this.businessException.getMessage()).isNotNull();
        assertThat(this.businessException.getLocalizedMessage()).isNotNull();
        assertThat(this.businessException.getStackTrace()).isNotNull();
    }

}

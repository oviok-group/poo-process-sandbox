/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : SandboxTechnicalExceptionTest.java
 * Date de création : 24 sept. 2020
 * Heure de création : 21:25:57
 * Package : fr.vincent.tuto.exceptions
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.exceptions;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Classe des tests unitaires des objets de type
 * [{@link SandboxTechnicalException}
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(name = "sandboxTechnicalExceptionTest", classes = { SandboxTechnicalException.class })
@SpringBootTest
public class SandboxTechnicalExceptionTest
{

    private SandboxTechnicalException technicalException;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        this.technicalException = new SandboxTechnicalException();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.technicalException = null;
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.exceptions.SandboxTechnicalException#SandboxTechnicalException()}.
     */
    @Test
    public void testSandboxTechnicalException()
    {
        // AssertJ assertion's
        assertThat(this.technicalException.getCause()).isNull();
        assertThat(this.technicalException.getMessage()).isNull();
        assertThat(this.technicalException.getLocalizedMessage()).isNull();
        assertThat(this.technicalException.getStackTrace()).isNotNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.exceptions.SandboxTechnicalException#SandboxTechnicalException(java.lang.String)}.
     */
    @Test
    public void testSandboxTechnicalExceptionString()
    {
        final String message = "Message Erreur Exception";
        technicalException = new SandboxTechnicalException(message);

        //
        assertThat(this.technicalException.getCause()).isNull();
        assertThat(this.technicalException.getMessage()).isEqualToIgnoringCase(message);
        assertThat(this.technicalException.getLocalizedMessage()).isNotNull();
        assertThat(this.technicalException.getStackTrace()).isNotNull();
    }

    @Test
    public void testSandboxTechnicalExceptionStringWithNull()
    {
        technicalException = new SandboxTechnicalException((String) null);

        // AssertJ assertion's
        assertThat(this.technicalException.getCause()).isNull();
        assertThat(this.technicalException.getMessage()).isNull();
        assertThat(this.technicalException.getLocalizedMessage()).isNull();
        assertThat(this.technicalException.getStackTrace()).isNotNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.exceptions.SandboxTechnicalException#SandboxTechnicalException(java.lang.String, java.lang.Throwable)}.
     */
    @Test
    public void testSandboxTechnicalExceptionStringThrowable()
    {
        final String message = "Message Erreur Exception";
        final Throwable throwable = new Throwable(message);
        technicalException = new SandboxTechnicalException(throwable);
        //
        assertThat(this.technicalException.getCause()).isNotNull();
        assertThat(this.technicalException.getCause().getMessage()).isEqualToIgnoringCase(message);
        assertThat(this.technicalException.getCause()).isNotNull();
        assertThat(this.technicalException.getLocalizedMessage()).isNotNull();
        assertThat(this.technicalException.getStackTrace()).isNotNull();
    }

    @Test
    public void testSandboxTechnicalExceptionStringThrowableWithNull()
    {
        technicalException = new SandboxTechnicalException((Throwable) null);

        // AssertJ assertion's
        assertThat(this.technicalException.getCause()).isNull();
        assertThat(this.technicalException.getMessage()).isNull();
        assertThat(this.technicalException.getStackTrace()).isNotNull();
        assertThat(this.technicalException.getLocalizedMessage()).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.exceptions.SandboxTechnicalException#SandboxTechnicalException(java.lang.Throwable)}.
     */
    @Test
    public void testSandboxTechnicalExceptionThrowable()
    {
        final String message = "Message Erreur Exception";
        final Throwable throwable = new Throwable(message);
        technicalException = new SandboxTechnicalException(message, throwable);
        //
        assertThat(this.technicalException.getCause().getMessage()).isEqualToIgnoringCase(message);
        assertThat(this.technicalException.getCause()).isEqualTo(throwable);
        assertThat(this.technicalException.getStackTrace().length > 0).isEqualTo(true);
        assertThat(this.technicalException.getLocalizedMessage()).isNotNull();
        assertThat(this.technicalException.getStackTrace()).isNotNull();
    }

    @Test
    public void testSandboxTechnicalExceptionThrowableWithNull()
    {
        technicalException = new SandboxTechnicalException((String) null, (Throwable) null);
        //
        assertThat(this.technicalException.getCause()).isNull();
        assertThat(this.technicalException.getMessage()).isNull();
        assertThat(this.technicalException.getLocalizedMessage()).isNull();
        assertThat(this.technicalException.getStackTrace().length > 0).isEqualTo(true);
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.exceptions.SandboxTechnicalException#SandboxTechnicalException(java.lang.String, java.lang.Throwable, boolean, boolean)}.
     */
    @Test
    public void testSandboxTechnicalExceptionStringThrowableBooleanBoolean()
    {
        final String message = "Message Erreur Exception";
        final Throwable throwable = new Throwable(message);
        this.technicalException = new SandboxTechnicalException(message, throwable, true, true);

        //
        assertThat(this.technicalException.getCause()).isNotNull();
        assertThat(this.technicalException.getMessage()).isNotNull();
        assertThat(this.technicalException.getLocalizedMessage()).isNotNull();
        assertThat(this.technicalException.getStackTrace()).isNotNull();
    }

    @Test
    public void testSandboxTechnicalExceptionStringThrowableBooleanBooleanFalse()
    {
        final String message = "Message Erreur Exception";
        final Throwable throwable = new Throwable(message);
        this.technicalException = new SandboxTechnicalException(message, throwable, false, false);

        //
        assertThat(this.technicalException.getCause()).isNotNull();
        assertThat(this.technicalException.getMessage()).isNotNull();
        assertThat(this.technicalException.getLocalizedMessage()).isNotNull();
        assertThat(this.technicalException.getStackTrace()).isNotNull();
    }

}

/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : ProxyStaticContextUtilsTest.java
 * Date de création : 25 sept. 2020
 * Heure de création : 10:34:41
 * Package : fr.vincent.tuto.utils
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.utils;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.vincent.tuto.config.PooSandboxBaseConfig;
import fr.vincent.tuto.config.props.EnvPropsLoaderSandboxConfig;

/**
 * Classe des Tests Unitaires des objets de type {@link ProxyStaticContextUtils}
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = "classpath:poo-process-sandbox-test.properties")
@ContextConfiguration(name = "proxyStaticContextUtilsTest", classes = { PooSandboxBaseConfig.class,
        EnvPropsLoaderSandboxConfig.class, ProxyStaticContextUtils.class })
@SpringBootTest
@ActiveProfiles("test")
public class ProxyStaticContextUtilsTest
{

    /**
     * Test method for
     * {@link fr.vincent.tuto.utils.ProxyStaticContextUtils#getDataSourceProps()}.
     */
    @Test
    public void testGetDataSourceProps()
    {
        final Map<String, String> result = ProxyStaticContextUtils.getProps();

        assertThat(result).isNotNull();
        assertThat(result.isEmpty()).isFalse();
        assertThat(result.size()).isGreaterThan(0);

    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.utils.ProxyStaticContextUtils#getDataSourcePropsWithBeanName()}.
     */
    @Test
    public void testGetDataSourcePropsWithBeanName()
    {
        final Map<String, String> result = ProxyStaticContextUtils.getPropsWithBeanName();

        assertThat(result).isNotNull();
        assertThat(result.isEmpty()).isFalse();
        assertThat(result.size()).isGreaterThan(0);
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.utils.ProxyStaticContextUtils#getDictionnaireProps()}.
     */
    @Test
    public void testGetDictionnaireProps()
    {
        final Map<String, String> result = ProxyStaticContextUtils.getDictionnaireProps();
        assertThat(result).isNotNull();
        assertThat(result.isEmpty()).isFalse();
        assertThat(result.size()).isEqualTo(4);
    }

}

/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : SandboxUtilsTest.java
 * Date de création : 30 sept. 2020
 * Heure de création : 07:54:19
 * Package : fr.vincent.tuto.utils
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.utils;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import fr.vincent.tuto.exceptions.SandboxBusinessException;

/**
 * Classe des tests unitaires des objets de type {@link SandboxUtils}
 * 
 * @author Vincent Otchoun
 */
public class SandboxUtilsTest
{
    /**
     * Test method for
     * {@link fr.vincent.tuto.utils.SandboxUtils#calculerDureeExecution(long)}.
     * 
     * @throws InterruptedException
     */
    @Test
    public void testCalculerDureeExecution() throws InterruptedException
    {
        final long initail = System.nanoTime();
        calculation(2);
        final long resultat = SandboxUtils.calculerDureeExecution(initail);
        assertThat(Long.valueOf(resultat)).isNotNull();
    }

    @Test
    public void testCalculerDureeExecutionWithZero() throws InterruptedException
    {
        calculation(0);
        final long resultat = SandboxUtils.calculerDureeExecution(0);
        assertThat(resultat).isGreaterThan(0);
    }

    @Test
    public void testCalculerDureeExecutionWithNegatif() throws InterruptedException
    {
        final long initail = -System.nanoTime();
        // System.err.println(">>>>>><< initial : \n" + initail);
        calculation(2);
        final long resultat = SandboxUtils.calculerDureeExecution(initail);
        assertThat(resultat).isGreaterThan(0);
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.utils.SandboxUtils#calculerDureeWithInstant(long)}.
     * 
     * @throws InterruptedException
     */
    @Test
    public void testCalculerDureeWithInstant() throws InterruptedException
    {
        final long initail = Instant.now().toEpochMilli();
        calculation(2);
        final long resultat = SandboxUtils.calculerDureeWithInstant(initail);
        assertThat(Long.valueOf(resultat)).isNotNull();
        assertThat(resultat).isGreaterThan(0);
    }

    @Test
    public void testCalculerDureeWithInstantWithZero() throws InterruptedException
    {
        calculation(0);
        final long resultat = SandboxUtils.calculerDureeWithInstant(0);
        assertThat(Long.valueOf(resultat)).isNotNull();
        assertThat(resultat).isGreaterThan(0);
    }

    @Test
    public void testCalculerDureeWithInstantWithNegatif() throws InterruptedException
    {
        final long initail = -Instant.now().toEpochMilli();
        calculation(2);
        final long resultat = SandboxUtils.calculerDureeWithInstant(initail);
        assertThat(Long.valueOf(resultat)).isNotNull();
        assertThat(resultat).isGreaterThan(0);
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.utils.SandboxUtils#calculerDureeWithDate(long)}.
     * 
     * @throws InterruptedException
     */
    @Test
    public void testCalculerDureeWithDate() throws InterruptedException
    {
        final long initail = new Date().getTime();
        calculation(2);
        final long resultat = SandboxUtils.calculerDureeWithDate(initail);
        assertThat(Long.valueOf(resultat)).isNotNull();
        assertThat(resultat).isGreaterThan(0);
    }

    @Test
    public void testCalculerDureeWithZero() throws InterruptedException
    {
        calculation(0);
        final long resultat = SandboxUtils.calculerDureeWithDate(0);
        assertThat(Long.valueOf(resultat)).isNotNull();
        assertThat(resultat).isGreaterThan(0);
    }

    @Test
    public void testCalculerDureeWithNegatif() throws InterruptedException
    {
        final long initail = -new Date().getTime();
        calculation(2);
        final long resultat = SandboxUtils.calculerDureeWithDate(initail);
        assertThat(Long.valueOf(resultat)).isNotNull();
        assertThat(resultat).isGreaterThan(0);
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.utils.SandboxUtils#comparerDuree(long, long, java.lang.String, java.lang.String)}.
     */
    @Test
    public void testComparerDuree()
    {
        final long resultat = SandboxUtils.comparerDuree(10l, 22l, "A", "B");

        assertThat(resultat).isGreaterThan(0);
    }

    @Test
    public void testComparerDuree_2()
    {

        final long resultat = SandboxUtils.comparerDuree(50l, 35l, "A", "B");

        assertThat(resultat).isGreaterThan(0);
    }

    @Test(expected = SandboxBusinessException.class)
    public void testComparerDureeWithNameNull()
    {
        final long resultat = SandboxUtils.comparerDuree(10l, 22l, null, null);

        assertThat(resultat).isGreaterThan(0);
    }

    @Test(expected = SandboxBusinessException.class)
    public void testComparerDureeWithNameEmpty()
    {
        final String methodeA = StringUtils.EMPTY;
        final String methodeB = StringUtils.EMPTY;

        final long resultat = SandboxUtils.comparerDuree(10l, 22l, methodeA, methodeB);

        assertThat(resultat).isGreaterThan(0);
    }

    @Test(expected = SandboxBusinessException.class)
    public void testComparerDureeWithNegatif()
    {
        final String methodeA = "A";
        final String methodeB = "B";

        final long resultat = SandboxUtils.comparerDuree(-10l, -22l, methodeA, methodeB);

        assertThat(resultat).isGreaterThan(0);
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.utils.SandboxUtils#calculerDimensionTableauEntiers(int[][], java.lang.Boolean)}.
     */
    @Test
    public void testCalculerDimensionTableauEntiersLignes()
    {
        final int[][] TABLEAU = new int[30][30];
        final int resultat = SandboxUtils.calculerDimensionTableauEntiers(TABLEAU, Boolean.TRUE);
        assertThat(resultat).isGreaterThan(0);
        assertThat(resultat).isEqualTo(30);
    }

    @Test
    public void testCalculerDimensionTableauEntiersColonnes()
    {
        final int[][] TABLEAU = new int[30][50];
        final int resultat = SandboxUtils.calculerDimensionTableauEntiers(TABLEAU, Boolean.FALSE);
        assertThat(resultat).isGreaterThan(0);
        assertThat(resultat).isEqualTo(50);
    }

    @Test(expected = SandboxBusinessException.class)
    public void testCalculerDimensionTableauEntiersWithNull()
    {
        final int resultat = SandboxUtils.calculerDimensionTableauEntiers(null, Boolean.TRUE);
        assertThat(resultat).isGreaterThan(0);
    }

    private static void calculation(int pSleep) throws InterruptedException
    {
        // Sleep 2 seconds
        TimeUnit.NANOSECONDS.sleep(2);

    }

}

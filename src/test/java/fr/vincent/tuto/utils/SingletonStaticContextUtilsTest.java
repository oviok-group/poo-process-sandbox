/*
 * ----------------------------------------------
 * Projet ou Module : poo-process-sandbox
 * Nom de la classe : SingletonStaticContextUtilsTest.java
 * Date de création : 25 sept. 2020
 * Heure de création : 23:13:45
 * Package : fr.vincent.tuto.utils
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.utils;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.vincent.tuto.config.PooSandboxBaseConfig;
import fr.vincent.tuto.config.props.EnvPropsLoaderSandboxConfig;

/**
 * Classe des tests unitares des objets de type
 * {@link SingletonStaticContextUtils }
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = "classpath:poo-process-sandbox-test.properties")
@ContextConfiguration(name = "singletonStaticContextUtilsTest", classes = { PooSandboxBaseConfig.class,
        EnvPropsLoaderSandboxConfig.class, SingletonStaticContextUtils.class })
@SpringBootTest
@ActiveProfiles("test")
public class SingletonStaticContextUtilsTest
{

    /**
     * Test method for
     * {@link fr.vincent.tuto.utils.SingletonStaticContextUtils#getProps()}.
     */
    @Test
    public void testGetProps()
    {
        final Map<String, String> result = SingletonStaticContextUtils.getProps();

        assertThat(result).isNotNull();
        assertThat(result.isEmpty()).isFalse();
        assertThat(result.size()).isGreaterThan(0);
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.utils.SingletonStaticContextUtils#getPropsWithBeanName()}.
     */
    @Test
    public void testGetPropsWithBeanName()
    {
        final Map<String, String> result = SingletonStaticContextUtils.getPropsWithBeanName();

        assertThat(result).isNotNull();
        assertThat(result.isEmpty()).isFalse();
        assertThat(result.size()).isGreaterThan(0);
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.utils.SingletonStaticContextUtils#getDictionnaireProps()}.
     */
    @Test
    public void testGetDictionnaireProps()
    {
        final Map<String, String> result = SingletonStaticContextUtils.getDictionnaireProps();
        assertThat(result).isNotNull();
        assertThat(result.isEmpty()).isFalse();
        assertThat(result.size()).isEqualTo(4);

        /*
         * 3+4
         * 7
         * 34
         */
        // System.err.println("3 + 4");
        // System.err.println(3 + 4);
        // System.err.println(3 + "4");
    }

}
